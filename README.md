# Paypal App Lambda

## Architectural Flow
![Flow](flow.drawio.png)

## Installation
- `cd client && npm i && cd ../server && npm i`

## Deployment To SIT
- `cd server && npm run deploy:sit`

## Appreciation
Visit my Medium and follow me: https://medium.com/@cheahwen1997

