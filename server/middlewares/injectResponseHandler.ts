export const injectResponseHandler = () => ({
  after: async handler => {
    handler.response = {
      statusCode: 200,
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': 'OPTIONS,DELETE,GET,HEAD,PATCH,POST,PUT',
      },
      body: JSON.stringify({
        success: true,
        data: handler.response || 'empty response',
        meta: null,
      }),
    };

    return handler.response;
  },
  onError: async handler => {
    const { error } = handler;

    handler.response = {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': 'OPTIONS,DELETE,GET,HEAD,PATCH,POST,PUT',
      },
      body: JSON.stringify({
        success: false,
        data: null,
        meta: {
          statusCode: error.statusCode || 500,
          error: error.message || error || 'Internal Server Error',
        },
      }),
    };
  },
});
