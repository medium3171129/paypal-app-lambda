import * as Paypal from '@paypal/checkout-server-sdk';
import middy from '@middy/core';
import { APIGatewayProxyEvent } from 'aws-lambda';
import middlewares from '../../../middlewares';
import { AmountBreakdown } from '@paypal/checkout-server-sdk/lib/payments/lib';
import catalogs from '../../../data/catalogs.json';
import Joi from 'joi';

const paypalIntentHandler = async (_event: APIGatewayProxyEvent) => {
  if (!_event.body) throw new Error(`Event body is empty`);

  let parsedBody = JSON.parse(_event.body);

  const schema = Joi.object<{
    ids: Number[];
  }>().keys({
    ids: Joi.array().items(Joi.string().required()).required(),
  });

  const { error, value } = schema.validate(parsedBody);

  if (error) {
    throw {
      statusCode: 400,
      message: error,
    };
  }

  if (!Array.isArray(parsedBody.ids)) throw new Error(`Specified ids is not an array`);

  let targetCatalogs = catalogs.filter(c => parsedBody.ids.includes(c.id));
  if (targetCatalogs.length <= 0) throw new Error(`No catalogs found`);

  // This sample uses SandboxEnvironment. In production, use LiveEnvironment
  const paypalEnv = new Paypal.core.SandboxEnvironment(process.env.PAYPAL_CLIENT_ID, process.env.PAYPAL_CLIENT_SECRET);
  const paypalClient = new Paypal.core.PayPalHttpClient(paypalEnv);

  const paypalOrderRequest = new Paypal.orders.OrdersCreateRequest();

  paypalOrderRequest.prefer('return=representation');

  paypalOrderRequest.requestBody({
    intent: 'CAPTURE',
    purchase_units: [
      {
        amount: {
          currency_code: 'USD',
          value: targetCatalogs.reduce((total, item) => total + Number(item.price), 0).toString(),
          breakdown: {
            item_total: {
              currency_code: 'USD',
              value: targetCatalogs.reduce((total, item) => total + Number(item.price), 0).toString(),
            },
          } as AmountBreakdown,
        },
        items: targetCatalogs.map(c => ({
          name: `${c.name} (${c.code})`,
          unit_amount: {
            currency_code: 'USD',
            value: c.price,
          },
          quantity: '1',
          category: 'DIGITAL_GOODS',
        })),
      },
    ],
  });

  const order = await paypalClient.execute(paypalOrderRequest);

  return { orderId: order.result.id };
};

export const handler = middy(paypalIntentHandler).use(middlewares());
