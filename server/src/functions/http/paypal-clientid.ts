import middy from '@middy/core';
import { APIGatewayProxyEvent } from 'aws-lambda';
import middlewares from '../../../middlewares';

export const paypalClientIdHandler = async (_event: APIGatewayProxyEvent) => ({
  clientId: process.env.PAYPAL_CLIENT_ID,
});

export const handler = middy(paypalClientIdHandler).use(middlewares());
