import middy from '@middy/core';
import { APIGatewayProxyEvent } from 'aws-lambda';
import middlewares from '../../../middlewares';
import catalogs from '../../../data/catalogs.json';

export const catalogsHandler = async (_event: APIGatewayProxyEvent) => ({
  catalogs,
});

export const handler = middy(catalogsHandler).use(middlewares());
