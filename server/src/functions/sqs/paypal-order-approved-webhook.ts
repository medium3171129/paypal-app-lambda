import middy from '@middy/core';
import { SQSEvent } from 'aws-lambda';
import middlewares from '../../../middlewares';

export const PaypalOrderApprovedWebhookhandler = async (_event: SQSEvent) => {
  // do some db update stuff
  console.log(_event.Records);
  return _event.Records;
};

export const handler = middy(PaypalOrderApprovedWebhookhandler).use(middlewares());
