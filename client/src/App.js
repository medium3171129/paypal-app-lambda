import { Col, Row } from "antd";
import Catalogs from "./Catalogs";
import Checkout from "./Checkout";
import "./App.css";
import { useEffect, useState } from "react";
import registerFetchInterceptor from "./registerInterceptor";

function App() {
  const [selectedItems, setSelectedItems] = useState([]);

  useEffect(() => {
    registerFetchInterceptor();
  }, []);

  return (
    <div className="App">
      <Row
        gutter={20}
        style={{ width: "100%", height: "100%" }}
        align="top"
        justify="center"
      >
        <Col span={12} style={{ width: "100%", height: "80%" }}>
          <Catalogs
            selectedItems={selectedItems}
            setSelectedItems={setSelectedItems}
          />
        </Col>
        <Col span={12} style={{ width: "100%", height: "80%" }}>
          <Checkout
            selectedItems={selectedItems}
            setSelectedItems={setSelectedItems}
          />
        </Col>
      </Row>
    </div>
  );
}

export default App;
