import { message } from "antd";
import fetchIntercept from "fetch-intercept";

const DEFAULT_TIMEOUT = 30000; // 30 seconds request timeout

function registerFetchInterceptor() {
  fetchIntercept.register({
    request: async function (url, config) {
      console.log(config)

      if (config) {config.signal = AbortSignal.timeout(DEFAULT_TIMEOUT);
      
        // add api key
        console.log(config)
      }

      return [url, config];
    },

    requestError: function (error) {
      // Called when an error occured during another 'request' interceptor call
      console.log(error);
      return Promise.reject(error);
    },

    response: function (response) {
      // Modify the reponse object
      if (!response.ok) {
        console.log(response)
        if (response.status === 404) message.error(`API does not exist`);
        else if (response?.status && response?.statusText)
          message.error(`Code ${response?.status}: ${response?.statusText}`);
      }
      return response;
    },

    responseError: function (error) {
      // Handle an fetch error
      if (error instanceof DOMException && error.name === "AbortError") {
        message.error("Timeout");
        return Promise.reject(new Error("Timeout"));
      } else message.error(error?.message ?? "Unknown Error");

      return Promise.reject(error);
    },
  });
}

export default registerFetchInterceptor;
